from django import forms
from .models import DocumentModel

class DocumentForm(forms.Form):
    docfile = forms.FileField(
        label='Select a file'
    )