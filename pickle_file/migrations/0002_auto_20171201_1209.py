# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-12-01 06:39
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pickle_file', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='description',
        ),
        migrations.RemoveField(
            model_name='document',
            name='uploaded_at',
        ),
    ]
