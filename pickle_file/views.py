from django.shortcuts import render,redirect
from pickle_file.forms import DocumentForm
from .models import DocumentModel
from django.template import RequestContext
from django.http import HttpResponse
from wsgiref.util import FileWrapper
import pickle
import wget
# import os
# import ctypes
# import urllib.request

def base(request):
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = DocumentModel(docfile=request.FILES['docfile'])
            newdoc.save()
            path='media/documents/'
            f = open(path + 'cordinates.txt')
            bubble_dict = {}
            for i in f.readlines():
                lines = i.split(';')
                q_no = lines[0]
                # print q_no
                bubble_dict[q_no] = eval(lines[1])
            pickle.dump(bubble_dict, open(path + 'ref_coord_a.p', 'wb'))
            # url = 'media/documents/ref_coord_a.p'
            # response = urllib.request.urlopen(url)
            # data = response.read()  # a `bytes` object
            # text = data.decode('utf-8')
            # print('Beginning file download with urllib2...')

            # print "downloading with urllib"
            # urllib.urlretrieve(url, "code.zip")


            # Redirect to the document list after POST
            return render(request,'pickle_file/base.html')
    else:
        form = DocumentForm()  # A empty, unbound form

    # Load documents for the list page
    documents = DocumentModel.objects.last()
    context_instance = RequestContext(request)

    # Render list page with the documents and the form
    return render(request,
        'pickle_file/base.html',
        {'documents': documents, 'form': form},context_instance

    )
# def download(url):
#     url = 'media/documents/ref_coord_a.p'
#     return HttpResponse (wget.download(url, '/Downloads/'))

