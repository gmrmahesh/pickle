from __future__ import unicode_literals
from django.db import models

class DocumentModel(models.Model):
    docfile = models.FileField(upload_to='documents/')